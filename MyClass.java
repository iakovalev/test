public class MyClass {

	private int n;
	private String str;
	
	public void MyClass(){
	}
	
	public void MyClass(int n, String str){
		this.n = n;
		this.str = str;
	}
	
	public int getN(){
	return n;
	}
	
	public void setN(int n){
	this.n = n;
	}
	
		public String getStr(){
		return str;
	}
	
	public void setStr(String str){
		this.str = str;
	}
	
	@Override
	public boolean equals(Object obj){
		
		if(obj == null) return false;
		
		if(!(obj instanceof myClass)) return false;
		
		MyClass myClass = (myClass) obj;
		
		return myClass.getN == this.n && myClass.getStr == this.str;
	}
	
}